Yet Another Blog Archive
https://www.drupal.org/project/blog_archive
==============================================

INTRODUCTION
------------

Provides "Archive Block" as blogger.com for core blog module.

REQUIREMENTS
------------
blog (core)
views

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------

Add block "Blog Archive" to your theme region

MAINTAINERS
-----------

Current maintainers:
* Andrey Ivnitsky - https://www.drupal.org/u/itcrowd72
