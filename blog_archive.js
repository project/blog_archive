/**
 * @file
 * The js file which collapse elements.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.blogArchive = {
    attach: function (context, settings) {
      // Cache var
      var $archive_block = $('.archive-block');
      // Prevent multiple runs
      $($archive_block).once(function () {
        // Close all UL's
        $('ul', $archive_block).each(function () {
          $(this).hide();
        });
        // Open first UL
        $archive_block.find('h3').first().children('.collapse-icon').text('▼');
        $archive_block.find('ul').first().show();
        // onClick
        $('h3', $archive_block).click(function () {
          var icon = $(this).children('.collapse-icon');
          $(this).siblings('ul').slideToggle(function () {
            if (icon.text() === '▼') {
              icon.text('►');
            }
            else {
              icon.text('▼');
            }
          });
        });
      });
    }
  };

})(jQuery);
